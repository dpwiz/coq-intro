From mathcomp Require Import ssreflect.

Axiom replace_with_your_solution_here : forall {A : Type}, A.

(* Remember function from the first lecture *)

Definition foo := fun (f : bool -> bool) => f true.

(* We can generalize it a bit *)

Definition applyb (* : bool -> (bool -> bool) -> bool  *)
  := fun (b : bool) (f : bool -> bool) => f b.

Definition apply
  :
    forall A B, A -> (A -> B) -> B
  :=
    fun (A B : Type) =>
      fun (a : A) =>
        fun (f : A -> B) =>
          f a
.

Compute apply bool bool true negb.
Compute apply nat nat 4 S.

(* implement polymorphic version of the apply funtion *)
(* Say about `forall` erasing *)

(* implement parameterized inductive of prod3 *)

(* Inductive prod3 ... : ... :=
  | triple ... *)

Inductive prod3 (A B C : Type) : Type :=
  | triple : A -> B -> C -> prod3 A B C
  .

(* Without Coq try to infer `prod3`'s and `triple`'s type *)

Check prod3.
Check triple.

Arguments triple [A B C] _ _ _.

Check triple 1 2 true.

(* Make implicit some `apply`'s and `triple`'s arguments *)
(* Say about alternative way of Implicit's  *)

(* Introduce a haskell like `$` notation *)

Section Haskell.

(* Local Notation .. := .. . *)

End Haskell.

(* Introduce a (a; b; c) notation for triple *)

(* Notation .. := .. *)


(* function composition *)

Definition comp A B C (f : B -> A) (g : C -> B)  : C -> A
:= replace_with_your_solution_here.


(* Introduce a notation that lets us use composition like so: f \o g.
   You might need to tweak the implicit status of some comp's arguments *)


(* implement functions with the given signatures *)

Definition prodA (A B C : Type) :
  (A * B) * C -> A * (B * C)
(* := fun '((a, b), c) => (a, (b, c)). *)
:=
  fun abc =>
    (* match abc with
    | pair ab c =>
        match ab with
        | pair a b =>
            pair a (pair b c)
        end
    end *)
    match abc with
    | pair (pair a b) c =>
        pair a (pair b c)
    end
  .

Definition sumA (A B C : Type) :
  (A + B) + C -> A + (B + C)
:=
  fun abc =>
    match abc with
    | inl (inl a) => inl a
    | inl (inr b) => inr (inl b)
    | inr c       => inr (inr c)
    end.

Definition prod_sumD (A B C : Type) :
  A * (B + C) -> (A * B) + (A * C)
:=
  fun '(a, bc) =>
    match bc with
    | inl b => inl (a, b)
    | inr c => inr (a, c)
    end.

Definition sum_prodD (A B C : Type) :
  A + (B * C) -> (A + B) * (A + C)
:=
  fun a'bc =>
    match a'bc with
    | inl a => (inl B a, inl C a)
    | inr (b, c) => (inr A b : A + B, inr A c : A + C)
    end.

(* Introduce `unit` type (a type with exactly one canonical form) *)

(* () : () *)
Inductive unit : Type :=
  | tt.

Definition unit_initial (A : Type) :
  A -> unit
:= fun _ => tt.

(* Introduce empty type, call `void` *)

Inductive void : Type := .

Definition void_terminal (A : Type) :
  void -> A
:= fun absurd => match absurd with end.


(* Think of some more type signatures involving void, unit, sum, prod *)
