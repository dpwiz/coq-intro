From mathcomp Require Import ssreflect ssrfun ssrbool eqtype ssrnat div.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


(* SSReflect tactic practice *)

Section IntLogic.

Variables A B C : Prop.

(** * Exercise *)
Lemma andA :
  (A /\ B) /\ C -> A /\ (B /\ C).
Proof.
case.
case.
move => a b c.
split.
- exact: a.
split.
- exact: b.
- exact: c.
Qed.

Lemma andA' :
  (A /\ B) /\ C -> A /\ (B /\ C).
Proof.
case.
case.
move => a b c.
split.
exact: a.
split.
exact: b.
exact: c.
Defined.

About andA.

Compute andA.

About andA'.

Compute andA'.


(** * Exercise *)
Lemma conj_disjD :
  A /\ (B \/ C) -> (A /\ B) \/ (A /\ C).
Proof.
case.
move => a.
case.
- move => b.
  left.
  split.
  exact: a.
  exact: b.
- move => c.
  right.
  split.
  exact: a.
  exact: c.
Qed.

(** * Exercise *)
Lemma disj_conjD :
  A \/ (B /\ C) -> (A \/ B) /\ (A \/ C).
Proof.
case.
- move => a.
  split.
  - left.
    exact: a.
  - left.
    exact: a.
- case.
  move => b c.
  split.
  - right. exact: b.
  - right. exact: c.
Qed.
(** * Exercise *)
Lemma notTrue_iff_False :
  (~ True) <-> False.
Proof.
split.
- move => nt. case nt. exact.
- move => f. case f.
Qed.
(** Hint 1: use [case] tactic on a proof of [False] to apply the explosion
principle. *)
(** Hint 2: to solve the goal of the form [True], use [exact: I], or simple
automation. *)


(** * Exercise *)

Lemma imp_trans :
  (A -> B) -> (B -> C) -> (A -> C).
Proof.
move => ab bc a.
apply (bc (ab a)).
Qed.

(** * Exercise *)
Lemma dne_False :
  ~ ~ False -> False.
Proof.
(* do ! apply. *)
rewrite /not.
apply.
done.
Qed.

(** * Exercise *)
Lemma dne_True :
  ~ ~ True -> True.
Proof.
rewrite / not.
move => _.
exact: I.
(* done. *)
(* move => nnt.
exact. *)
Qed.

(** * Exercise *)
Lemma DNE_triple_neg :
  ~ ~ ~ A -> ~ A.
Proof.
move => nnna.
move => a.
apply : nnna.
done.
Qed.
(** Hint : use `apply: (identifier)`
to apply a hypothesis from the context to
the goal and keep the hypothesis in the context *)

End IntLogic.

(* Lemma poop (A : Prop):
  (A \/ ~ A) -> True.
Proof.
(* move => _. *)
done.
Qed. *)

(* Lemma boop (A : Prop): *)

Lemma flip (A B : Prop):
  ~ (A \/ B) -> (~ A /\ ~ B).
Proof.
Admitted.

(** * Exercise *)
Lemma nlem (A : Prop):
  ~ (A \/ ~ A) -> A.
Proof.
move => n'aVna.
have : (~ A /\ ~ ~ A).
(* - apply n'aVna. *)
rewrite / not.
(* move => H. *)
(* move => H. *)
Admitted.
(** Hint: you might want to use a separate lemma here to make progress.
Or, use the `have` tactic: `have: statement` creates a new subgoal and asks
you to prove the statement. This is like a local lemma. *)


(** * Exercise *)
Lemma weak_Peirce (A B : Prop) :
  ((((A -> B) -> A) -> A) -> B) -> B.
Proof.
Admitted.


(** * Exercise *)
(* Prove that having a general fixed-point combinator in Coq would be incosistent *)
Definition FIX := forall A : Type, (A -> A) -> A.

Lemma fix_inconsistent :
  FIX -> False.
Proof.
Admitted.

Section Boolean.
(** * Exercise *)
Lemma negbNE b : ~~ ~~ b -> b.
Proof.
rewrite / is_true.
move : b.
case; by move => /=. (* WTF: /= *)
(* case; [done|done]. *)
Qed.

(** * Exercise *)
Lemma negbK : involutive negb.
Proof.
rewrite / involutive.
rewrite / cancel.
by case.
Qed.

(** * Exercise *)
Lemma negb_inj : injective negb.
Proof.
rewrite / injective.
do ? case. done. done. done. done.
(* - case => /= [x|y]. *)
(* - move => a. *)
(* case; by case. *)
Qed.

End Boolean.

Section EquationalReasoning.

Variables A B : Type.

Locate "_ =1 _".
Print eqfun.

Locate "_ = _".

(** * Exercise 10 *)
Lemma eqext_refl (f : A -> B) :
  f =1 f.
Proof.
  (* rewrite / eqfun. *)
  move => a.
  exact: erefl.
  (* rewrite
  split. *)
Qed.

(** * Exercise 11 *)
Lemma eqext_sym (f g : A -> B) :
  f =1 g -> g =1 f.
Proof.
do 2! rewrite / (_ =1 _).
move => fg.
move => x.

rewrite (fg x).
(* by rewrite fg. *)

apply erefl.
(* done. *)

Qed.
(** Hint: `rewrite` tactic also works with
universally quantified equalities. I.e. if you
have a hypothesis `eq` of type `forall x, f x = g
x`, you can use `rewrite eq` and Coq will usually
figure out the concrete `x` it needs to specialize
`eq` with, meaning that `rewrite eq` is
essentially `rewrite (eq t)` here. *)


(** * Exercise *)
Lemma eqext_trans (f g h : A -> B) :
  f =1 g -> g =1 h -> f =1 h.
Proof.
do 3! rewrite / (_ =1 _).

move => fg gh.
move => x.

by rewrite -gh.
(* by rewrite fg. *)
(* by rewrite fg gh. *)

(* rewrite fg.
rewrite gh.
apply erefl. *)
(* done. *)
Qed.

End EquationalReasoning.

Lemma AA : associative addn.
Proof.
rewrite /associative.
move => x y z.
move : x.
elim; [done|].
move => x IHx.
(* Search (_ .+1 + _). *)
do ? rewrite addSn.
rewrite IHx.
done.
Qed.

(** * Exercise *)
Lemma and_via_ex (A B : Prop) :
  (exists (_ : A), B) <-> A /\ B.
Proof.

Admitted.


(** * Exercise *)
(* Hint: the `case` tactic understands constructors are injective *)
Lemma pair_inj A B (a1 a2 : A) (b1 b2 : B) :
  (a1, b1) = (a2, b2) -> (a1 = a2) /\ (b1 = b2).
Proof.
(* by case. *)
case.
(* move => H1 H2.
rewrite H1 H2. *)
move => -> ->.
split; exact: erefl.
Qed.


(** * Exercise *)
Lemma false_eq_true_implies_False :
  forall n, n.+1 = 0 -> False.
Proof.
done.
(* move => n. *)
Qed.


(** * Exercise *)
Lemma addn0 :
  right_id 0 addn.
Proof.
rewrite / right_id.
elim.
- done.
move => n.
move => IH.
(* Search (_ .+1 + _ = _). *)
by rewrite addSn IH.
Qed.

(** * Exercise *)
Lemma addnS :
  forall m n, m + n.+1 = (m + n).+1.
Proof.
  move => m n.
  move : m.
  elim.
  - done.
  move => m IHn.
  do ? rewrite addSn.
  rewrite IHn.
  done.
Qed.


(** * Exercise: *)
Lemma addnCA : left_commutative addn.
Proof.
rewrite / left_commutative.
move => x y z.
move : y.
elim.
- done.
move => n IHn.
do ? rewrite addSn.
rewrite addnS.
rewrite IHn.
done.
Qed.

(** * Exercise: *)
Lemma addnC : commutative addn.
Proof.
rewrite / commutative.
move => x y.
move : x.
elim.
- rewrite addn0.
  move: y.
  elim.
  - done.
  move => n IHn.
  - rewrite addnS IHn.
    done.
- move => n IHn.
  rewrite addnS.
  rewrite addSn.
  rewrite IHn.
  done.
Qed.

(* Variable A : Type. *)
(* Implicit Types x y z : A. *)

Lemma eq_sym (x y : Set) :
  x = y -> y = x.
Proof.
move => ->.
done.
Qed.

(** * Exercise (optional): *)
Lemma unit_neq_bool:
  unit <> bool.
Proof.
(* rewrite / not. *)
have : forall b, b = b.
- done.
move => bb.
rewrite /not.

Admitted.

(** [==] is the decidable equality operation for types with decidable equality.
    In case of booleans it means [if and only if]. *)
    Fixpoint mostowski_equiv (a : bool) (n : nat) :=
      if n is n'.+1 then mostowski_equiv a n' == a
      else a.

    (** The recursive function above encodes the following classical
        propositional formula:
        [((A <-> A ...) <-> A) <-> A]
        For instance, if n equals 3 then the formula look like this
        [((A <-> A) <-> A) <-> A]
        Since we work in the classical propositional fragment
        we can encode the [<->] equivalence with boolean equality [==].
     *)
    Print odd.
    (** Try to come up with a one-line proof *)
Lemma mostowski_equiv_even_odd a n :
  mostowski_equiv a n = a || odd n.
Proof.
Admitted.

(** Write a tail-recursive variation of the [addn] function
    (let's call it [addn_iter]).
    See https://en.wikipedia.org/wiki/Tail_call
 *)

Fixpoint add_iter (n m : nat) {struct n}: nat
  :=
    if n is n' .+1 then
      add_iter n' (m .+1)
    else
      m
  .

Lemma add_iter_correct n m :
  add_iter n m = n + m.
Proof.
move : n.
elim : m.

- elim.
  - done.
  - move => n IHn.
    rewrite /=.
    rewrite addSn IHn.
move => n.
  move
  (* rewrite addn0. *)

  admit.

move => n IHn m /=.
rewrite addSn /=.
rewrite -addnS.
rewrite IHn.


elim: n m => [|n IHn /=] m.
- done.
(* move => n IHn. *)
rewrite addSn /=.
rewrite -addnS.
rewrite IHn.
done.
Qed.

Lemma double_inj m n :
  m + m = n + n -> m = n.
Proof.
Admitted.

Lemma nat_3k5m n : exists k m, n + 8 = 3 * k + 5 * m.
Proof. Admitted.